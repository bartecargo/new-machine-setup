#!/bin/bash
set -e
set -x

#Recommended sources.list:
#deb https://deb.debian.org/debian/ testing main
#deb-src https://deb.debian.org/debian/ testing main
#deb http://security.debian.org testing-security main
#deb-src http://security.debian.org testing-security main
#deb https://deb.debian.org/debian/ testing-updates main
#deb-src https://deb.debian.org/debian/ testing-updates main

#Run as root
if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit
fi

#Add packages
apt update
apt install -y software-properties-common
apt-add-repository non-free
apt-add-repository contrib
apt update
apt -y autoremove
apt -y full-upgrade
apt -y install \
    apt-transport-https \
    aptitude \
    atop \
    autojump \
    build-essential \
    ca-certificates \
    clang \
    cmake \
    command-not-found \
    cups \
    cups-browsed \
    curl \
    debian-security-support \
    debsums \
    dlocate \
    docker.io \
    editorconfig \
    etckeeper \
    ffmpeg \
    firmware-linux \
    firmware-linux-nonfree \
    fzf \
    gnupg2 \
    gimp \
    git \
    git-crypt \
    golang \
    gron \
    gscan2pdf \
    hardinfo \
    hexedit \
    highlight \
    html-xml-utils \
    htop \
    httpie \
    iftop \
    intel-microcode \
    inxi \
    iotop \
    jq \
    keepassxc \
    lshw \
    lsof \
    lua5.2 \
    mitmproxy \
    mono-devel \
    ncdu \
    nmap \
    nmon \
    node-js-beautify \
    node-typescript \
    nodejs \
    npm \
    obs-studio \
    p7zip-full \
    powerline \
    putty \
    python3-pip \
    python3-dev \
    ranger \
    remmina \
    ripgrep \
    ruby \
    rxvt-unicode \
    saidar \
    samba \
    screenfetch \
    silversearcher-ag \
    smbclient \
    snapd \
    stterm \
    sudo \
    testssl.sh \
    texlive \
    texlive-latex-extra \
    tidy \
    tmux \
    trash-cli \
    tree \
    vim-gtk \
    vim-nox \
    virt-manager \
    vuls \
    whois \
    wireshark \
    xclip \
    youtube-dl \
    zsh

# Install Microsoft repos
wget -qO- https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > microsoft.asc.gpg
mv microsoft.asc.gpg /etc/apt/trusted.gpg.d/
echo "# Microsoft APT sources
deb [arch=amd64] https://packages.microsoft.com/debian/10/prod buster main
deb [arch=amd64] https://packages.microsoft.com/repos/azure-cli/ buster main
deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main" |
    tee /etc/apt/sources.list.d/microsoft.list

# Install Microsoft products
apt update
apt install -y dotnet-sdk-2.1 dotnet-sdk-3.1 azure-cli code

# Update all NPM packages
npm update -g

# Install Python packages
pip3 install --upgrade sshch

# Install antibody for zsh and set up profile to load bash profile
curl -sfL git.io/antibody | sh -s - -b /usr/local/bin
echo "emulate sh -c 'source /etc/profile'" > ~/.zprofile

# Install libsecret Git credential manager
apt install -y libsecret-1-dev glib-2.0
pushd /usr/share/doc/git/contrib/credential/libsecret
make
popd

# Install rstudio
apt -y install libxml2-dev r-base libcurl4-openssl-dev
wget $(curl -Ss https://rstudio.com/products/rstudio/download/ | hxnormalize -x | hxselect "td a[href^=\"https://download1.rstudio.org/desktop/bionic/amd64/\"][href\$=\"-amd64.deb\"]::attr(href)" -c) -O rstudiodebian.deb
apt install -y ./rstudiodebian.deb
rm ./rstudiodebian.deb

# Install Microsoft fonts
pushd /usr/share/fonts
mkdir -p microsoft
cd microsoft
wget https://bitbucket.org/bartj/new-machine-setup/raw/master/Debian/fonts.7z
7za x fonts.7z -y
rm fonts.7z
popd

# Install Powerline10k font
for fonttype in Regular Bold Italic Bold%20Italic
do
    mkdir -p /usr/share/fonts/powerline10k
    wget "https://github.com/romkatv/dotfiles-public/raw/master/.local/share/fonts/NerdFonts/MesloLGS%20NF%20$fonttype.ttf" -P /usr/share/fonts/powerline10k
done

# Install Beyond Compare
curl https://www.scootersoftware.com/RPM-GPG-KEY-scootersoftware | apt-key add -
echo 'deb https://www.scootersoftware.com/ bcompare4 non-free' > /etc/apt/sources.list.d/scootersoftware.list
apt update
apt install -y bcompare

# List unmanaged packages
echo 'The following packages cannot be automatically updated:'
aptitude search '~o'

echo 'The following packages are no longer supported:'
check-support-status

echo 'The following packages are missing their hash file:'
debsums -l

echo 'The following packages may be corrupt:'
debsums -c

echo "Root installation successful; now run per-user installation as yourself."
