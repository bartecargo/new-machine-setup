#requires -runasadministrator
#requires -version 4

$ErrorActionPreference = 'Stop'

function EnableDeveloperMode
{
    reg add "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\AppModelUnlock" /t REG_DWORD /f /v "AllowDevelopmentWithoutDevLicense" /d "1"
    if(!$?) {
        throw "Failed to enable developer mode."
    }
}

function EnableVirtualTerminalEscapeSequences
{
    Set-ItemProperty HKCU:\Console VirtualTerminalLevel -Type DWORD 1
}

function UseUtcInRTC
{
    Set-ItemProperty -Path "HKLM:\SYSTEM\CurrentControlSet\Control\TimeZoneInformation" -Name "RealTimeIsUniversal" -Type DWord -Value 1
}

function DisableInternetExplorerAutomaticProxyDetection
{
    Set-ItemProperty 'HKLM:\SOFTWARE\Policies\Microsoft\Windows\CurrentVersion\Internet Settings' -Name ProxySettingsPerUser -Value 1
    $defaultSettings = [byte[]](70, 0, 0, 0, 220, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
    Set-ItemProperty 'HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Internet Settings\Connections' -Name DefaultConnectionSettings -Value $defaultSettings
    Set-ItemProperty 'HKCU:\Software\Microsoft\Windows\CurrentVersion\Internet Settings' -Name AutoDetect -Value 0
}

function ExcludeDataFromRealtimeVirusScanning
{
    if((gp 'HKLM:\SOFTWARE\Policies\Microsoft\Windows Defender\' DisableAntiSpyware -ea 0 | select -expand DisableAntiSpyware) -eq 1) { return }

    Add-MpPreference -ExclusionPath D:\
}

function InstallVisualStudio()
{
    $upgrader = "C:\Program Files (x86)\Microsoft Visual Studio\Installer\vs_installer.exe"
    $vs2019Path = "C:\Program Files (x86)\Microsoft Visual Studio\2019\Professional"
    if((Test-Path $upgrader) -and (Test-Path $vs2019Path)) {
        $upgraderProcess = Start-Process -PassThru -Wait -FilePath $upgrader -ArgumentList 'update', '--passive', '--norestart', '--installpath', "`"$vs2019Path`""
        if($upgraderProcess.ExitCode -ne 0) {
            throw "Upgrade of Visual Studio failed with exit code $($upgraderProcess.ExitCode)."
        }
        return
    }

    $installer = Join-Path $env:Temp VisualStudioSetup.exe
    Invoke-WebRequest "https://download.visualstudio.microsoft.com/download/pr/321da3ad-5224-41dc-80e8-dd7610460f41/f75bf30b88b8218ed3e63b0d7ddf06d8/vs_professional.exe" -OutFile $installer
    $components =
        "Microsoft.VisualStudio.Component.CoreEditor",
        "Microsoft.VisualStudio.Workload.Data",
        "Microsoft.VisualStudio.Workload.ManagedDesktop",
        "Microsoft.VisualStudio.Workload.NativeDesktop",
        "Microsoft.VisualStudio.Workload.NetCoreTools",
        "Microsoft.VisualStudio.Workload.NetWeb",
        "Microsoft.Net.Component.4.7.SDK",
        "Microsoft.Net.Component.4.7.TargetingPack",
        "Microsoft.Net.Component.4.7.1.SDK",
        "Microsoft.Net.Component.4.7.1.TargetingPack",
        "Microsoft.Net.Component.4.7.2.SDK",
        "Microsoft.Net.Component.4.7.2.TargetingPack"
    $installerArguments = "--wait", "--norestart", "--passive", "--includeRecommended"
    $components | % {
        $installerArguments += "--add"
        $installerArguments += $_
    }
    $installerProcess = Start-Process -PassThru -Wait -FilePath $installer -ArgumentList $installerArguments
    if($installerProcess.ExitCode -ne 0) {
        throw "Installation of Visual Studio failed with exit code $($installerProcess.ExitCode)."
    }
}

function KillGitProcessThatStopsUpdatesFromWorking()
{
    Get-Process -ea 0 ssh-agent | Stop-Process -Force
}

function SetExecutionPolicy()
{
    Set-ExecutionPolicy RemoteSigned
}

function ConfigureWindowsUpdateToDownloadFromLocalPcs()
{
    $path = "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\DeliveryOptimization\Config"
    if (-not (Test-Path $path))
    {
        New-Item -Force $path | Out-Null
    }
    Set-ItemProperty -Path $path -Name DODownloadMode -Type DWord -Value 1
}

function EnableLongPaths()
{
    Set-ItemProperty 'HKLM:\System\CurrentControlSet\Control\FileSystem' -Name 'LongPathsEnabled' -value 1
}

function EnsureChocolateyInstalled()
{
    if(!(Test-Path $Profile)) { New-Item -Force -ItemType file $Profile } # if $profile doesn't exist, Chocolatey won't load its module
    if(!(gcm cinst -ErrorAction SilentlyContinue)) {
        iex ((new-object net.webclient).DownloadString('https://chocolatey.org/install.ps1'))
        $env:path += "$($env:SYSTEMDRIVE)\chocolatey\bin"
        . $Profile
    }
    choco feature enable -name=exitOnRebootDetected
}

function SetHighPerformancePowerProfile()
{
    powercfg -setactive 8c5e7fda-e8bf-4a96-9a85-a6e23a8c635c
    if(!$?) {
        throw "Failed to set power plan to 'High performance'."
    }
}

function InstallChromeExtensions()
{
    $alreadyInstalled = ls -ea 0 (join-path ([Environment]::GetFolderPath('LocalApplicationData')) 'Google\Chrome\User Data\Default\Extensions') | select -expand Name

    'dbepggeogbaibhgnhhndojpepiihcmeb', <# Vimium #>
    'febilkbfcbhebfnokafefeacimjdckgl', <# Markdown Preview Plus #>
    'mpbpobfflnpcgagjijhmgnchggcjblin', <# HTTP/2 and SPDY indicator #>
    'ecanpcehffngcegjmadlcijfolapggal', <# IPv6 indicator #>
    'cjpalhdlnbpafiamejdnhcphjbkeiagm', <# uBlock Origin #>
    'chklaanhfefbnpoihckbnefhakgolnmc', <# JSONView #>
    'jiokjpcfnipnicblkgdklknjjeohnfln', <# Google Analytics debugger #>
    'fmkadmapgofadopljbjfkapdkoienihi', <# React Developer Tools #>
    'mhjhnkcfbdhnjickkkdbjoemdmbfginb', <# Selector gadget #>
    'bfbameneiokkgbdmiekhjnmfkcnldhhm' <# Web Developer toolbar #> | % {
        if($alreadyInstalled -notcontains $_) {
            & "C:\Program Files (x86)\Google\Chrome\Application\Chrome.exe" --install-chrome-app=$_
        }
    }
}

function InstallFromChocolatey()
{
    $tools =
        '7zip',
        '7zip.commandline',
        'ag',
        'azure-cli',
        'azure-functions-core-tools',
        'beyondcompare',
        'cmake',   # for Vim/YouCompleteme
        'cpu-z',
        'crystaldiskmark',
        'dependencywalker',
        'dotnetcore-sdk',
        'editorconfig.core',
        'ffmpeg',
        'fiddler',
        'filezilla',
        'Firefox',
        'fzf',
        'gimp',
        'git /GitAndUnixToolsOnPath /NoAutoCrlf',
        'global',   # For Pascal autocomplete in VS Code - and maybe others (GNU Global)
        'GoogleChrome',
        'graphviz.portable',
        'hxd',
        'html-tidy',
        'ilspy',
        'jetbrainstoolbox',
        'jq',
        'keepass',
        'kitty.portable',
        'libreoffice-fresh',
        'linqpad5.anycpu.install',
        'lua53',    # for Vim
        'logparser',
        'microsoft-r-open',
        'miktex',
        'mitmproxy',
        'mremoteng',
        'ncrunch-console',
        'ncrunch-vs2019',
        'nmap',
        'nodejs-lts',
        'nuget.commandline',
        'openjdk',
        'pandoc',
        'perfview',
        'python3',
        'poshgit',
        'postman',
        'rdcman',
        'r.studio',
        'rsat',
        'rufus',
        'screentogif',
        'slack',
        'sql-operations-studio',
        'sqlgrep',
        'sqlitebrowser',
        'sysinternals',
        'treesizefree',
        'vim-tux /InstallPopUp /RestartExplorer',
        'visualstudiocode',
        'windbg',
        'windows-adk-all', # primarily for Windows Performance Analyzer
        'windows-admin-center',
        'wireshark',
        'xsltproc'

    $chocPath = Join-Path $env:ChocolateyInstall 'choco.exe'
    $alreadyInstalled = & $chocpath list --local-only | ? { ($_ -split ' ').length -eq 2 } | % { ($_ -split ' ')[0] }
    $newTools = $tools | ? { $alreadyInstalled -notcontains ($_ -split ' ', 2)[0] }

    function RebootIfRequired($chocolateyExitCode) {
        if($chocolateyExitCode -ne 350) {
            return
        }
        Write-Warning "Your computer needs to reboot to complete a previous software installation. Please reboot and restart this script."
        Start-Sleep -Seconds 2147483
        Restart-Computer
    }

    Write-Host "Updating all currently installed components..."
    Update-SessionEnvironment
    & $chocPath upgrade all -y
    RebootIfRequired $LASTEXITCODE
    Update-SessionEnvironment

    Write-Host "Installing all new components..."
    $newTools | % {
        $package, $packageParameters = $_ -split ' ', 2

        if($packageParameters) {
            & $chocPath install -y $package -params $packageParameters
        }
        else {
            & $chocPath install -y $package
        }

        RebootIfRequired $LASTEXITCODE
        Update-SessionEnvironment
    }
}

function UpgradeGlobalNpmPackages
{
    npm upgrade -g
}

function InstallPackagesUsingPythonPip
{
    pip install httpie
}

function InstallPackagesUsingWindowsPackageManagement()
{
    Get-PackageSource PSGallery | Set-PackageSource -Trusted
    Install-PackageProvider -Name NuGet -Force

    if(Get-Package -ea 0 PSVSEnv) { Uninstall-Package PSVSEnv }
    Install-Package -AllowClobber -Source psgallery PSVSEnv
}

function UpdatePowerShellProfile()
{
    $profileComment = "# Installed by Bart's BitBucket script"

    if((cat $profile) -contains $profileComment) {
        return
    }

    $commands = @($profileComment, "if(gmo -list Hyper-V) { ipmo Hyper-V }", "vs2019", $profileComment) -join "`r`n"

    Add-Content -Path $profile -Value "`r`n`r`n$commands`r`n"
}

function UpdatePowerShellHelp()
{
    Update-Help -Force -ErrorAction Continue
}

function CheckForResharperSettings()
{
    $fileName = 'ResharperSettings.DotSettings'
    $jetBrainsDirectory = Join-Path (Resolve-Path ~\AppData\Roaming) JetBrains
    $filePath = Join-Path $jetBrainsDirectory $fileName

    if(!(Test-Path $jetBrainsDirectory)) {
        Write-Host "ReSharper does not appear to be installed. Skipping configuration."
        return
    }

    if(ls $jetBrainsDirectory\Shared\vAny\GlobalSettingsStorage.DotSettings -ea 0 | sls $fileName) {
        Write-Host "ReSharper settings appear to already be in place."
    }

    $wc = New-Object System.Net.WebClient
    $wc.DownloadFile("https://bitbucket.org/bartj/new-machine-setup/raw/HEAD/WindowsDevelopment/$fileName", $filePath)
    Write-Warning "You will need to manually install the ReSharper settings file at $filePath."
}

function DisplayOptionalResharperSettings()
{
    'Potential ReSharper manual configuration:',
    '($DTE.Commands | ? { $_.Name -eq "ReSharper.ReSharper_DuplicateText" }).Bindings = @()' | % { Write-Host -ForegroundColor Green $_ }
}

function ConfigureGit()
{
    $beyondCompareExePath = "C:/Program Files/Beyond Compare 4/bcomp.exe"

    git config --global rebase.autoSquash true
    git config --global core.autocrlf true
    git config --global core.longpaths true
    git config --global core.symlinks true
    git config --global core.whitespace cr-at-eol
    git config --global difftool.prompt false
    git config --global mergetool.keepbackup false
    git config --global mergetool.prompt false
    git config --global receive.denynonfastforwards true    # mostly suitable for servers
    git config --global core.logallrefupdates true          # mostly suitable for servers
    git config --global diff.colorMoved zebra
    git config --global diff.algorithm histogram
    git config --global diff.renameLimit 10000
    git config --global http.sslbackend schannel

    git config --global diff.tool bc
    git config --global difftool.bc.path $beyondCompareExePath
    git config --global mergetool.bc.path $beyondCompareExePath
    git config --global merge.tool bc-custom
    git config --global mergetool.bc-custom.cmd ('\"' + $beyondCompareExePath + '\" \"$LOCAL\" \"$REMOTE\" \"$BASE\" -fv=\"Text Merge\" -mergeoutput=\"$MERGED\" -nobackups')

    git config --global difftool.rider.cmd '\"C:/Windows/System32/WindowsPowerShell/v1.0/powershell.exe\" -NoProfile -Command \"start -Wait (ls -r ~/AppData/Local/JetBrains/Toolbox/apps/Rider -fi rider64.exe | sort | select -last 1 -expand FullName) -ArgumentList ''diff'', ''\\\"$LOCAL\\\"'', ''\\\"$REMOTE\\\"''\"'
    git config --global mergetool.rider.cmd '\"C:/Windows/System32/WindowsPowerShell/v1.0/powershell.exe\" -NoProfile -Command \"start -Wait (ls -r ~/AppData/Local/JetBrains/Toolbox/apps/Rider -fi rider64.exe | sort | select -last 1 -expand FullName) -ArgumentList ''merge'', ''\\\"$LOCAL\\\"'', ''\\\"$REMOTE\\\"'', ''\\\"$BASE\\\"'', ''\\\"$MERGED\\\"''\"'
}

function ConfigureDocumentGeneration()
{
    python -m pip install --upgrade pip pygments

    $path = [Environment]::GetEnvironmentVariable('PATH', 'Machine')
    $scriptPath = Join-Path ([System.IO.Path]::GetDirectoryName((gcm python | select -expand Source))) 'Scripts'
    if($path -notlike ("*" + $scriptPath + "*")) {
        [Environment]::SetEnvironmentVariable('PATH', "$PATH;$scriptPath", 'Machine')
    }

    Update-SessionEnvironment
}

function InstallVim()
{
    npm install -g js-beautify typescript

    if(!(Test-Path ~\vimfiles)) {
        pushd ~
        try {
            git clone https://bartj@bitbucket.org/bartj/vim.git vimfiles
            cmd /c mklink _vimrc vimfiles\vimrc
            cmd /c mklink _gvimrc vimfiles\gvimrc
        }
        finally {
            popd
        }
    }

    # YouCompleteMe needs CMake
    if($env:path -notmatch 'cmake') {
        $env:path+=';C:\Program Files\CMake\bin'
        Write-Host -ForegroundColor Green "Added CMake to the  path for the current process."
    }

    git -C (Resolve-Path ~/vimfiles) pull --autostash --rebase
    $vim = ls 'C:\Program Files\vim' -r -fi gvim.exe | sort DirectoryName | select -last 1 -expand FullName
    start $vim '-c ":PlugInstall" -c ":PlugUpdate" -c ":PlugClean!" -c "qa"' -Wait
}

function UpgradeMiktex
{
    $mpmExe = ls "C:\Program Files\miktex*" -r -fi mpm.exe | select -expand FullName | sort | select -Last 1

    if (-not $mpmExe) {
        Write-Error "mpm.exe not found. Miktex probably wasn't installed correctly. Fix this and try again."
    }

    & $mpmExe --verbose --update --admin
    & $mpmExe --verbose --update
}

function InstallZ
{
    Install-Module Z -AllowClobber
    Update-Module Z
}

function ConfigureVisualStudioCode
{
    code --install-extension EditorConfig.EditorConfig
}

function DisableSomeTelemetry
{
    [Environment]::SetEnvironmentVariable('VSCMD_SKIP_SENDTELEMETRY', "true", 'Machine')   #in response to https://developercommunity.visualstudio.com/content/problem/694847/vsdevcmdbat-developer-prompt-causes-the-input-line.html
}

'EnableDeveloperMode',
'EnableVirtualTerminalEscapeSequences',
'UseUtcInRTC',
'DisableInternetExplorerAutomaticProxyDetection',
'ExcludeDataFromRealtimeVirusScanning',
'InstallVisualStudio',
'KillGitProcessThatStopsUpdatesFromWorking',
'SetExecutionPolicy',
'ConfigureWindowsUpdateToDownloadFromLocalPcs',
'EnableLongPaths',
'SetHighPerformancePowerProfile',
'EnsureChocolateyInstalled',
'InstallFromChocolatey',
'UpgradeGlobalNpmPackages',
'InstallPackagesUsingPythonPip',
'InstallPackagesUsingWindowsPackageManagement',
'UpdatePowerShellProfile',
'UpdatePowerShellHelp',
'ConfigureGit',
'InstallChromeExtensions',
'CheckForResharperSettings',
'DisplayOptionalResharperSettings',
'ConfigureDocumentGeneration',
'InstallVim',
'InstallZ',
'ConfigureVisualStudioCode',
'DisableSomeTelemetry',
'UpgradeMiktex' | % {
    Write-Host -ForegroundColor Green $_
    & $_
}
